let number;

number = Number(prompt("Please enter a number :"));
	console.log("The number you provided is " + number);
	// console.log(typeof number);

for(let counter = number ; counter >=50; counter--){
	if (counter % 10 === 0 && counter !==50){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}
	if (counter === 50){
		console.log("The current value is 50. Terminating the loop.");
		break;
	}
	if (counter % 5 === 0){
		console.log(counter);
		continue;
	}
}

function multiplicationTable(){
	for(let x = 1; x <=10; x++){
		let total = 5 * x;
		console.log( "5 x " + x + " = " + total );
	}
}
multiplicationTable();